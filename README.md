# resolve-validator

Template를 이용한 간단 validator.

String, Object 지원

## 1.0.5
- default 옵션 함수 지원

## 1.0.4
- this 지원

## 1.0.0

- 초기화
- @bitrading 그룹 이름 지원
