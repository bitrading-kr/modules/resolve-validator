/**
 * @fileOverview Validate with template
 * @name index.js
 * @author 2018 https://bitrading.kr
 * @copyright 2018 https://bitrading.kr
 * @created 2018-10-21
 * @version 1.0.5
 */
const _ = require('lodash'),
      defaults = require('defaults'),
      validator = require('validator'),
      mergeOptions = require('merge-options'),
      assertArgs = require('assert-args');

const VERSION="1.0.5";

const Validators = {
  'alpha': validator.isAlpha,
  'alphanumeric': validator.isAlphanumeric,
  'ascii': validator.isAscii,
  'base64': validator.isBase64,
  'before': validator.isBefore,
  'boolean': validator.isBoolean,
  'bytelength': validator.isByteLength,
  'creditcard': validator.isCreditCard,
  'currency': validator.isCurrency,
  'datauri': validator.isDataURI,
  'magneturi': validator.isMagnetURI,
  'decimal': validator.isDecimal,
  'divisibleby': validator.isDivisibleBy,
  'email': validator.isEmail,
  'empty': validator.isEmpty,
  'fqdn': validator.isFQDN,
  'float': validator.isFloat,
  'fullwidth': validator.isFullWidth,
  'halfwidth': validator.isHalfWidth,
  'hash': validator.isHash,
  'hexcolor': validator.isHexColor,
  'hexadecimal': validator.isHexadecimal,
  'identitycard': validator.isIdentityCard,
  'ip': validator.isIP,
  'iprange': validator.isIPRange,
  'isbn': validator.isISBN,
  'issn': validator.isISSN,
  'isin': validator.isISIN,
  'iso8601': validator.isISO8601,
  'rfc3339': validator.isRFC3339,
  'iso31661alpha2': validator.isISO31661Alpha2,
  'iso31661alpha3': validator.isISO31661Alpha3,
  'isrc': validator.isISRC,
  'int': validator.isInt,
  'json': validator.isJSON,
  'jwt': validator.isJWT,
  'latlong': validator.isLatLong,
  'length': validator.isLength,
  'lowercase': validator.isLowercase,
  'macaddress': validator.isMACAddress,
  'md5': validator.isMD5,
  'mimetype': validator.isMimeType,
  'mobilephone': validator.isMobilePhone,
  'mongoid': validator.isMongoId,
  'multibyte': validator.isMultibyte,
  'numeric': validator.isNumeric,
  'port': validator.isPort,
  'postalcode': validator.isPostalCode,
  'surrogatepair': validator.isSurrogatePair,
  'url': validator.isURL,
  'uuid': validator.isUUID,
  'uppercase': validator.isUppercase,
  'variablewidth': validator.isVariableWidth,
  'whitelisted': validator.isWhitelisted
};

class Validator {
  constructor (template) {
    let types = {},
        defaults = {},
        sanitizers = {},
        validators = {},
        converters = {};
    _.forEach(template, (value, key) => {
      if (_.isString(value.alias) && _.isPlainObject(template[value.alias])) {
        value = template[value.alias];
      }

      if (!_.isString(value.type) && !_.isArray(value.type)) throw new TypeError(`'${key}'의 'type'이 문자열이나 배열이 아닙니다.`);
      _.set(types, key, value.type);

      if (value.default !== undefined) {
        _.set(defaults, key, value.default);
      }

      if (value.sanitizer !== undefined) {
        if (!_.isFunction(value.sanitizer)) throw new TypeError(`'${key}'의 'sanitizer'가 함수가 아닙니다.`);
        _.set(sanitizers, key, value.sanitizer);
      }

      if (value.validator !== undefined) {
        if (!_.isFunction(value.validator) && !_.isString(value.validator)) throw new TypeError(`'${key}'의 'validator'이 문자열이나 함수가 아닙니다.`);
        _.set(validators, key, value.validator);
      }

      if (value.converter !== undefined) {
        if (!_.isFunction(value.converter)) throw new TypeError(`'${key}'의 'converter'가 함수가 아닙니다.`);
        _.set(converters, key, value.converter);
      }
    });

    this.data = {
      types: types,
      defaults: defaults,
      sanitizers: sanitizers,
      validators: validators,
      converters: converters
    };
  }

  getRecipe (key) {
    return {
      type: this.data.types[key],
      default: this.data.defaults[key],
      sanitizer: this.data.sanitizers[key],
      validator: this.data.validators[key],
      converter: this.data.converters[key]
    };
  }
}

class Resolver {
  constructor (template) {
    this.validator = new Validator(template);
  }

  resolve (key, value, options={}) {
    if (_.isPlainObject(key)) {
      return _.mapValues(_.cloneDeep(key), (_value, _key) => {
        return this._resolveItem(_value.key, _value.value, _value.options);
      });
    } else {
      return this._resolveItem(key, value, options);
    }
  }

  _resolveItem (key, value, options={}) {
    const recipe = mergeOptions(this.validator.getRecipe(key), options);

    // Type check
    if (!recipe.type) throw new Error(`'${key}'의 'type'을 찾을 수 없습니다.`);

    // Set default
    if (!this._checkType(value, recipe.type)) {
      if (recipe.default === undefined) throw new TypeError(`'${key}'의 값('${value}')이 '${recipe.type}' 타입이 아닙니다.`);
      if (_.isFunction(recipe.default)) {
        return recipe.default.call(this, value);
      } else {
        return recipe.default; // default 값은 바로 리턴
      }
    }

    // Sanitizer
    value = this._setSanitizer(value, recipe.sanitizer);

    // Validator
    if (recipe.validator !== undefined) {
      if (!this._checkValidator(value, recipe.validator)) {
        throw new TypeError(`'${key}'의 값('${value}')이 '${recipe.validator}' 검증에 실패하였습니다.`);
      }
    }

    // Converter
    if (_.isFunction(recipe.converter)) {
      value = recipe.converter.call(this, value);
    }

    return value;
  }

  _checkType (value, type) {
    try {
      assertArgs([value], {
        value: type
      });
      return true;
    } catch (e) {
      return false;
    }
  }

  _setSanitizer (value, sanitizer) {
    if (_.isFunction(sanitizer)) {
      return sanitizer.call(this, value);
    } else {
      return value;
    }
  }

  _checkValidator (value, _validator) {
    if (_.isString(_validator)) {
      let v = Validators[_validator];
      if (_.isFunction(v)) return v(value);
      return true;
    } else if (_.isFunction(_validator)) {
      return _validator.call(this, value, Validators);
    } else {
      return true;
    }
  }
}

module.exports = template => {
  let resolver = new Resolver(template);
  return function (...args) {
    return resolver.resolve.apply(resolver, args);
  };
};
