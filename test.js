const resolveValidator = require('./index.js'),
      assert = require('assert');

const Template = {
  'type-test': {
    type: 'string'
  },
  'multitype-test': {
    type: ['string', 'number']
  },
  'default-test': {
    type: 'string',
    default: 'hey'
  },
  'alias-test': {
    alias: 'default-test'
  },
  'sanitizer-test': {
    type: 'string',
    default: 'hey',
    sanitizer: v => {
      return v+'abcd';
    }
  },
  'validator-test': {
    type: 'string',
    default: 'hey',
    sanitizer: v => {
      return v+'abcd';
    },
    validator: v => {
      return v.slice(-4) == 'abcd';
    }
  },
  'validator-error-test': {
    type: 'string',
    default: 'hey',
    sanitizer: v => {
      return v+'abcd';
    },
    validator: v => {
      return v.slice(-4) != 'abcd';
    }
  },
  'converter-test': {
    type: 'string',
    default: 'hey',
    sanitizer: v => {
      return v+'abcd';
    },
    validator: v => {
      return v.slice(-4) == 'abcd';
    },
    converter: v => {
      return v.replace("abcd", "efgh");
    },
  }
};

let resolver;

describe('Generate', () => {
  describe(`#.resolveValidator()`, function () {
    it(`#(template) => `, function () {
      assert.doesNotThrow(() => resolver = resolveValidator(Template));
    });
  });
});

describe(`Resolve`, function () {
  describe(`Using string`, function () {
    it(`#('type-test', 'abcd') => 'abcd'`, function () {
      assert.equal(resolver('type-test', 'abcd'), 'abcd');
    });
    it(`#('type-test', ['abcd']) => Error`, function () {
      assert.throws(() => resolver('type-test', ["ABCD"]));
    });
    it(`#('multitype-test', 'abcd') => 'abcd'`, function () {
      assert.equal(resolver('multitype-test', 'abcd'), 'abcd');
    });
    it(`#('multitype-test', 1234) => 1234`, function () {
      assert.equal(resolver('multitype-test', 1234), 1234);
    });
    it(`#('multitype-test', ['abcd']) => Error`, function () {
      assert.throws(() => resolver('multitype-test', ["ABCD"]));
    });
    it(`#('default-test', 'hi') => 'hi'`, function () {
      assert.equal(resolver('default-test', 'hi'), 'hi');
    });
    it(`#('default-test', null) => 'hey'`, function () {
      assert.equal(resolver('default-test', null), 'hey');
    });
    it(`#('alias-test', null) => 'hey'`, function () {
      assert.equal(resolver('alias-test', null), 'hey');
    });
    it(`#('sanitizer-test', 'hello') => 'helloabcd'`, function () {
      assert.equal(resolver('sanitizer-test', 'hello'), 'helloabcd');
    });
    it(`#('sanitizer-test', null) => 'hey'`, function () {
      assert.equal(resolver('sanitizer-test', null), 'hey');
    });
    it(`#('validator-test', 'hello') => 'helloabcd'`, function () {
      assert.equal(resolver('validator-test', 'hello'), 'helloabcd');
    });
    it(`#('validator-error-test', 'hello') => 'helloabcd'`, function () {
      assert.throws(() => resolver('validator-error-test', 'hello'));
    });
    it(`#('converter-test', 'hello') => 'helloefgh'`, function () {
      assert.equal(resolver('converter-test', 'hello'), 'helloefgh');
    });
  });
  describe(`Using object`, function () {
    it(`'type-test', 'multitype-test'`, function () {
      let args = {
        test: {
          key: 'type-test',
          value: 'hihi'
        },
        hey: {
          key: 'multitype-test',
          value: 1
        },
        ho: {
          key: 'multitype-test',
          value: 'ac'
        }
      },
          res = {
            test: 'hihi',
            hey: 1,
            ho: 'ac'
          };
      assert.deepEqual(resolver(args), res);
    });
    it(`'default-test'`, function () {
      let args = {
        test: {
          key: 'default-test',
          value: null
        },
        hey: {
          key: 'default-test',
          value: 1
        },
        ho: {
          key: 'default-test',
          value: []
        }
      },
          res = {
            test: 'hey',
            hey: 'hey',
            ho: 'hey'
          };
      assert.deepEqual(resolver(args), res);
    });
    it(`'alias-test', 'sanitizer-test'`, function () {
      let args = {
        test: {
          key: 'alias-test',
          value: null
        },
        hey: {
          key: 'sanitizer-test',
          value: 'aaaa'
        }
      },
          res = {
            test: 'hey',
            hey: 'aaaaabcd',
          };
      assert.deepEqual(resolver(args), res);
    });
    it(`'validator-test', 'converter-test'`, function () {
      let args = {
        test: {
          key: 'validator-test',
          value: 'abcd'
        },
        hey: {
          key: 'converter-test',
          value: 'aaaa'
        }
      },
          res = {
            test: 'abcdabcd',
            hey: 'aaaaefgh',
          };
      assert.deepEqual(resolver(args), res);
    });
    it(`'validator-error-test'`, function () {
      let args = {
        test: {
          key: 'validator-error-test',
          value: 'abcd'
        }
      };
      assert.throws(() => resolver(args));
    });
  });
});
